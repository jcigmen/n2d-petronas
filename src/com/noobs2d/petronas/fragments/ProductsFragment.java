package com.noobs2d.petronas.fragments;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ListView;

import com.noobs2d.petronas.activities.ProductViewActivity;
import com.noobs2d.petronas.activities.PromoViewActivity;
import com.noobs2d.petronas.adapters.ProductsAndPromosListAdapter;
import com.noobs2d.petronas.listeners.ProductsAndPromosRetrievalListener;
import com.noobs2d.petronas.models.Product;
import com.noobs2d.petronas.models.Promotion;

/**
 * The <i>Products</i> tab.
 * 
 * @author Julious Cious Igmen <jcigmen@gmail.com>
 */
public class ProductsFragment extends ListFragment implements ProductsAndPromosRetrievalListener {

    private ArrayList<Object> collection = new ArrayList<Object>();

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
	super.onActivityCreated(savedInstanceState);
	setListAdapter(new ProductsAndPromosListAdapter(getActivity(), getActivity().getLayoutInflater(), collection));
	removeListDividers();
    }

    @Override
    public void onListItemClick(ListView listView, View v, int position, long id) {
	Object object = listView.getItemAtPosition(position);
	Intent intent = null;
	if (object instanceof Promotion) {
	    Promotion promo = (Promotion) object;
	    intent = new Intent(getActivity(), PromoViewActivity.class);
	    intent.putExtra("PROMO", promo);
	    startActivity(intent);
	} else {
	    Product product = (Product) object;
	    intent = new Intent(getActivity(), ProductViewActivity.class);
	    intent.putExtra("PRODUCT", product);
	    startActivity(intent);
	}
    }

    @Override
    public void onProductsRetrieved(ArrayList<Promotion> promos, ArrayList<Product> products) {
	collection.clear();
	collection.addAll(promos);
	collection.addAll(products);
    }

    @Override
    public void onProductsRetrieveFailed(Exception e) {
	if (isAdded()) {
	    setListAdapter(new ProductsAndPromosListAdapter(getActivity(), getActivity().getLayoutInflater()));
	    removeListDividers();
	}
    }

    private void removeListDividers() {
	getListView().setDivider(null);
	getListView().setDividerHeight(0);
    }
}
