package com.noobs2d.petronas.fragments;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ListView;

import com.noobs2d.petronas.activities.BranchViewActivity;
import com.noobs2d.petronas.adapters.BranchListAdapter;
import com.noobs2d.petronas.listeners.BranchesRetrievalListener;
import com.noobs2d.petronas.models.Branch;

/**
 * List of Branches.
 * 
 * @author Julious Cious Igmen <jcigmen@gmail.com>
 */
public class BranchesFragment extends ListFragment implements BranchesRetrievalListener {

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
	super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onBranchesRetrieved(ArrayList<Branch> branches) {
	if (isAdded()) {
	    setListAdapter(new BranchListAdapter(getActivity().getLayoutInflater(), branches));
	    removeListDividers();
	    getListView().setFastScrollEnabled(true);
	}
    }

    @Override
    public void onBranchesRetrieveFailed(Exception e) {
	if (isAdded()) {
	    setListAdapter(new BranchListAdapter(getActivity().getLayoutInflater()));
	    removeListDividers();
	}
    }

    @Override
    public void onListItemClick(ListView listView, View v, int position, long id) {
	Branch branch = (Branch) listView.getAdapter().getItem(position);

	Intent intent = new Intent(getActivity(), BranchViewActivity.class);
	intent.putExtra("BRANCH_ADDRESS", branch.location.toString());
	intent.putExtra("BRANCH_NAME", branch.name);
	intent.putExtra("BRANCH_CONTACT", branch.contact.toString());
	intent.putExtra("BRANCH_LATITUDE", branch.location.latitude);
	intent.putExtra("BRANCH_LONGITUDE", branch.location.longitude);
	startActivity(intent);
    }

    private void removeListDividers() {
	getListView().setDivider(null);
	getListView().setDividerHeight(0);
    }
}
