package com.noobs2d.petronas.fragments;

import java.util.ArrayList;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.noobs2d.petronas.MainActivity;
import com.noobs2d.petronas.R;
import com.noobs2d.petronas.listeners.BranchMapMarkerClickListener;
import com.noobs2d.petronas.models.Branch;
import com.noobs2d.petronas.utils.Utils;

/**
 * The <i>Near Me</i> tab. Shows a map and the nearest Petronas branch to the user.
 * 
 * @author Julious Cious Igmen <jcigmen@gmail.com>
 */
public class NearMeFragment extends Fragment implements LocationListener, LocationSource {

    /** We need to get the user's location outside this fragment so we adjust his to be static. */
    public static Location userLocation;
    private String bestAvailableProvider;
    private Criteria criteria = new Criteria();
    private OnLocationChangedListener listener;
    private LocationManager locationManager;
    private GoogleMap map;

    @Override
    public void activate(OnLocationChangedListener listener) {
	this.listener = listener;

	final long REQUEST_LOCATION_MIN_MILLIS = 1000; // millis before requesting the location update again
	final float REQUEST_LOCATION_MIN_DISTANCE = 0; // distance change before requesting the location update again

	// Request location updates from Location Manager
	if (bestAvailableProvider != null)
	    locationManager.requestLocationUpdates(bestAvailableProvider, REQUEST_LOCATION_MIN_MILLIS, REQUEST_LOCATION_MIN_DISTANCE, this);

	manuallyFireLocationChangeEvent();
    }

    @Override
    public void deactivate() {
	locationManager.removeUpdates(this);
	listener = null;
    }

    public Location getUserLocation() {
	return userLocation;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
	super.onActivityCreated(savedInstanceState);
	setUpMap();
	showNearestBranches();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	if (container == null)
	    return null;
	View view = inflater.inflate(R.layout.fragment_near_me, container, false);
	return view;
    }

    /** The mapfragment's id must be removed from the FragmentManager or else if the same it is passed on the next time then  app will crash */
    @Override
    public void onDestroyView() {
	super.onDestroyView();
	try {
	    if (map != null) {
		MainActivity.fragmentManager.beginTransaction().remove(MainActivity.fragmentManager.findFragmentById(R.id.map)).commit();
		map = null;
	    }
	} catch (IllegalStateException e) {
	}
    }

    @Override
    public void onLocationChanged(Location location) {
	if (listener != null)
	    listener.onLocationChanged(location);
	if (map != null)
	    map.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(), location.getLongitude())));
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    public void showNearestBranches() {
	if (map != null && getActivity() != null) {
	    userLocation = map.getMyLocation();
	    if (userLocation != null && MainActivity.branchesController != null) {
		ArrayList<Branch> branches = MainActivity.branchesController.getNearestBranches(userLocation);

		for (Branch branch : branches) {
		    MarkerOptions markerOptions = new MarkerOptions();
		    markerOptions.title(branch.name);
		    markerOptions.position(new LatLng(branch.location.latitude, branch.location.longitude));
		    markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.marker));
		    map.addMarker(markerOptions);
		}
		map.setOnMarkerClickListener(new BranchMapMarkerClickListener(map, getActivity().getLayoutInflater(), branches));
	    }
	}
    }

    /** To assess the user's best last known location. */
    private void manuallyFireLocationChangeEvent() {
	Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
	if (location != null)
	    onLocationChanged(location);
    }

    /**
     *  Sets up the map if it is possible to do so 
     */
    private void setUpMap() {
	locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
	criteria.setAccuracy(Criteria.ACCURACY_FINE);
	criteria.setPowerRequirement(Criteria.NO_REQUIREMENT);
	bestAvailableProvider = locationManager.getBestProvider(criteria, true);

	if (map == null) {
	    map = ((SupportMapFragment) MainActivity.fragmentManager.findFragmentById(R.id.map)).getMap();
	    if (map != null) {
		map.setLocationSource(this);
		map.setMyLocationEnabled(true);
		map.moveCamera(CameraUpdateFactory.zoomTo(12f));
		zoomToCurrentLocation();
	    }
	}
    }

    private void zoomToCurrentLocation() {
	userLocation = map.getMyLocation();
	if (userLocation != null)
	    map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(userLocation.getLatitude(), userLocation.getLongitude()), Utils.MAP_ZOOM_LEVEL));
    }
}
