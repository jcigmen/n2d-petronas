package com.noobs2d.petronas.activities;

import java.io.IOException;
import java.io.InputStream;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.noobs2d.petronas.R;
import com.noobs2d.petronas.models.Product;

public class ProductViewActivity extends Activity implements OnClickListener {

    @Override
    public void onClick(View view) {
	if (view.getId() == R.id.branchViewBack)
	    finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_product_view);

	((ImageView) findViewById(R.id.productViewBack)).setOnClickListener(this);

	Product product = (Product) getIntent().getSerializableExtra("PRODUCT");

	TextView name = (TextView) findViewById(R.id.productViewName);
	TextView category = (TextView) findViewById(R.id.productViewCategory);
	TextView description = (TextView) findViewById(R.id.productViewDescription);
	ImageView image = (ImageView) findViewById(R.id.productViewImage);

	name.setText(product.name);
	category.setText(product.category);
	description.setText(android.text.Html.fromHtml("<b>Description:</b><br/><br/>" + product.description));

	InputStream inputStream = null;
	try {
	    inputStream = getApplicationContext().getAssets().open(product.thumbnailFilename);
	} catch (IOException e) {
	    e.printStackTrace();
	}
	Drawable drawable = Drawable.createFromStream(inputStream, null);
	image.setImageDrawable(drawable);

    }
}
