package com.noobs2d.petronas.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.noobs2d.petronas.R;
import com.noobs2d.petronas.adapters.BranchMapItemMarkerAdapter;
import com.noobs2d.petronas.utils.Utils;

/**
 * What shows up when a branch from the list is tapped.
 * 
 * @author Julious Cious Igmen <jcigmen@gmail.com>
 */
public class BranchViewActivity extends FragmentActivity implements OnClickListener {

    @Override
    public void onBackPressed() {
	finish();
    }

    @Override
    public void onClick(View view) {
	if (view.getId() == R.id.branchViewBack)
	    finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_branch_view);

	final String branchAddress = getIntent().getStringExtra("BRANCH_ADDRESS");
	final String branchName = getIntent().getStringExtra("BRANCH_NAME");
	final String branchContact = getIntent().getStringExtra("BRANCH_CONTACT");
	float branchLatitude = getIntent().getFloatExtra("BRANCH_LATITUDE", 0f);
	float branchLongitude = getIntent().getFloatExtra("BRANCH_LONGITUDE", 0f);

	((TextView) findViewById(R.id.branchViewName)).setText(branchName);
	((ImageView) findViewById(R.id.branchViewBack)).setOnClickListener(this);

	GoogleMap map = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.branchViewMap)).getMap();
	if (map != null) {
	    MarkerOptions markerOptions = new MarkerOptions();
	    markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.marker));
	    markerOptions.position(new LatLng(branchLatitude, branchLongitude));
	    map.setMyLocationEnabled(true);
	    map.setInfoWindowAdapter(new BranchMapItemMarkerAdapter(getLayoutInflater(), branchName, branchAddress, branchContact));
	    map.addMarker(markerOptions).showInfoWindow();
	    map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(branchLatitude, branchLongitude), Utils.MAP_ZOOM_LEVEL));
	}
    }
}
