package com.noobs2d.petronas.activities;

import java.io.IOException;
import java.io.InputStream;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.noobs2d.petronas.R;
import com.noobs2d.petronas.models.Promotion;

public class PromoViewActivity extends Activity implements OnClickListener {

    @Override
    public void onClick(View view) {
	if (view.getId() == R.id.branchViewBack)
	    finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_promo_view);

	((ImageView) findViewById(R.id.promoViewBack)).setOnClickListener(this);

	Promotion promo = (Promotion) getIntent().getSerializableExtra("PROMO");

	TextView name = (TextView) findViewById(R.id.promoViewName);
	TextView tagline = (TextView) findViewById(R.id.promoViewTagline);
	TextView description = (TextView) findViewById(R.id.promoViewDescription);
	ImageView image = (ImageView) findViewById(R.id.promoViewImage);
	TextView period = (TextView) findViewById(R.id.promoViewPeriod);
	TextView permit = (TextView) findViewById(R.id.promoViewPermit);

	name.setText(promo.name);
	tagline.setText(promo.subName);
	description.setText(android.text.Html.fromHtml(promo.description));
	period.setText(promo.promoPeriod);
	permit.setText("Permit #: " + promo.permitNumber);

	InputStream inputStream = null;
	try {
	    inputStream = getApplicationContext().getAssets().open(promo.thumbnailFilename);
	} catch (IOException e) {
	    e.printStackTrace();
	}
	Drawable drawable = Drawable.createFromStream(inputStream, null);
	image.setImageDrawable(drawable);

    }
}
