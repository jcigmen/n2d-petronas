package com.noobs2d.petronas.adapters;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.noobs2d.petronas.R;
import com.noobs2d.petronas.models.Product;
import com.noobs2d.petronas.models.Promotion;

/**
 * @author Julious Cious Igmen <jcigmen@gmail.com>
 */
public class ProductsAndPromosListAdapter extends BaseAdapter {

    public static class ProductTag {

	public TextView name;
	public TextView subName;
	public ImageView thumbnail;
    }

    public static class PromotionTag {

	public ImageView banner;
	public TextView description;
	public TextView name;
	public TextView tagline;
    }

    private ArrayList<Object> collection;
    private Context context;
    private LayoutInflater inflater;

    public ProductsAndPromosListAdapter(Context context, LayoutInflater inflater) {
	this.context = context;
	this.inflater = inflater;
	collection = new ArrayList<Object>();
    }

    public ProductsAndPromosListAdapter(Context context, LayoutInflater inflater, ArrayList<Object> collection) {
	this.context = context;
	this.inflater = inflater;
	this.collection = collection;
    }

    @Override
    public int getCount() {
	return collection.size();
    }

    @Override
    public Object getItem(int position) {
	return collection.get(position);
    }

    @Override
    public long getItemId(int position) {
	return 0;
    }

    @Override
    public int getItemViewType(int position) {
	return getItem(position) instanceof Promotion ? 1 : 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
	if (getItem(position) instanceof Promotion)
	    convertView = inflatePromoItem(position, convertView, parent);
	else if (getItem(position) instanceof Product)
	    convertView = inflateProductItem(position, convertView, parent);

	return convertView;
    }

    @Override
    public int getViewTypeCount() {
	return 2;
    }

    private View inflateProductItem(int position, View convertView, ViewGroup parent) {
	ProductTag tag = null;
	if (convertView == null) {
	    convertView = inflater.inflate(R.layout.fragment_product_list, parent, false);
	    tag = new ProductTag();
	    tag.name = (TextView) convertView.findViewById(R.id.productName);
	    tag.subName = (TextView) convertView.findViewById(R.id.productSubname);
	    tag.thumbnail = (ImageView) convertView.findViewById(R.id.productThumbnail);
	    convertView.setTag(tag);
	} else
	    tag = (ProductTag) convertView.getTag();

	Product product = (Product) getItem(position);
	tag.name.setText(product.name);
	tag.subName.setText(product.subName);
	InputStream inputStream = null;
	try {
	    inputStream = context.getAssets().open(product.thumbnailFilename);
	} catch (IOException e) {
	    e.printStackTrace();
	}
	Drawable drawable = Drawable.createFromStream(inputStream, null);
	tag.thumbnail.setImageDrawable(drawable);
	return convertView;
    }

    private View inflatePromoItem(int position, View convertView, ViewGroup parent) {
	PromotionTag tag = null;
	if (convertView == null) {
	    convertView = inflater.inflate(R.layout.fragment_promo_list, parent, false);
	    tag = new PromotionTag();
	    tag.name = (TextView) convertView.findViewById(R.id.promoName);
	    tag.tagline = (TextView) convertView.findViewById(R.id.promoTagline);
	    tag.description = (TextView) convertView.findViewById(R.id.promoDescription);
	    tag.banner = (ImageView) convertView.findViewById(R.id.promoBanner);
	    convertView.setTag(tag);
	} else
	    tag = (PromotionTag) convertView.getTag();

	Promotion promo = (Promotion) getItem(position);
	tag.name.setText(promo.name);
	tag.tagline.setText(promo.subName);
	tag.description.setText(promo.description);
	InputStream inputStream = null;
	try {
	    inputStream = context.getAssets().open(promo.thumbnailFilename);
	} catch (IOException e) {
	    e.printStackTrace();
	}
	Drawable drawable = Drawable.createFromStream(inputStream, null);
	tag.banner.setImageDrawable(drawable);
	return convertView;
    }

}
