package com.noobs2d.petronas.adapters;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Set;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.noobs2d.petronas.R;
import com.noobs2d.petronas.models.Branch;
import com.noobs2d.petronas.utils.Utils;

/**
 * @author Julious Cious Igmen <jcigmen@gmail.com>
 */
public class BranchListAdapter extends BaseAdapter implements SectionIndexer {

    private class BranchTag {

	public Branch branch;
	public TextView branchAddress;
	public TextView branchName;
	public TextView branchPhoneContact;
	public ImageView near;
    }

    private class HeaderTag {

	public Branch branch;
	public TextView title;
    }

    private HashMap<String, Integer> alphaIndexer;
    private ArrayList<Branch> branches;
    private ArrayList<Integer> headerPositions;
    private LayoutInflater inflater;
    private String[] sections;

    public BranchListAdapter(LayoutInflater inflater) {
	this.inflater = inflater;
	branches = new ArrayList<Branch>();
    }

    public BranchListAdapter(LayoutInflater inflater, ArrayList<Branch> branches) {
	this.inflater = inflater;
	this.branches = branches;

	setUpIndexer();
    }

    @Override
    public int getCount() {
	return branches.size();
    }

    @Override
    public Branch getItem(int index) {
	return branches.get(index);
    }

    @Override
    public long getItemId(int position) {
	return Integer.valueOf(branches.get(position).id);
    }

    @Override
    public int getItemViewType(int position) {
	if (position == 0)
	    return 0;

	for (int i : headerPositions)
	    if (position == i)
		return 0;

	return 1;
    }

    @Override
    public int getPositionForSection(int position) {
	if (position >= alphaIndexer.size())
	    return 0;
	return alphaIndexer.get(sections[position]);
    }

    @Override
    public int getSectionForPosition(int position) {
	Branch branch = getItem(position);
	String cityFirstLetter = branch.location.city.length() > 0 ? branch.location.city.substring(0, 1) : "";
	for (int i = 0; i < sections.length; i++)
	    if (sections[i].equals(cityFirstLetter))
		return i;
	return 0;
    }

    @Override
    public Object[] getSections() {
	return sections;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
	boolean returnHeaderView = getItemViewType(position) == 0 || convertView != null && convertView.getTag() instanceof HeaderTag;
	if (returnHeaderView)
	    return getHeader(position, convertView, parent);
	return getListItemView(position, convertView, parent);
    }

    @Override
    public int getViewTypeCount() {
	return 2;
    }

    @Override
    public boolean hasStableIds() {
	return true;
    }

    private View getHeader(int position, View convertView, ViewGroup parent) {
	HeaderTag tag = new HeaderTag();
	if (convertView == null) {
	    convertView = inflater.inflate(R.layout.branch_list_header, parent, false);
	    tag.branch = getItem(position);
	    tag.title = (TextView) convertView.findViewById(R.id.branchListHeaderText);
	    convertView.setTag(tag);
	} else
	    tag = (HeaderTag) convertView.getTag();
	Branch branch = getItem(position);
	String cityFirstLetter = branch.location.city.length() > 0 ? branch.location.city.substring(0, 1) : "";
	tag.title.setText(cityFirstLetter);
	return convertView;
    }

    private View getListItemView(int position, View convertView, ViewGroup parent) {
	BranchTag tag = null;
	if (convertView == null) {
	    convertView = inflater.inflate(R.layout.fragment_branch_list, parent, false);
	    tag = new BranchTag();
	    tag.branch = getItem(position);
	    tag.branchAddress = (TextView) convertView.findViewById(R.id.branchAddress);
	    tag.branchName = (TextView) convertView.findViewById(R.id.branchName);
	    tag.branchPhoneContact = (TextView) convertView.findViewById(R.id.branchPhoneContact);
	    tag.near = (ImageView) convertView.findViewById(R.id.branchNear);
	    convertView.setTag(tag);
	} else
	    tag = (BranchTag) convertView.getTag();

	Branch branch = getItem(position);
	tag.branchAddress.setText(branch.location.city);
	tag.branchName.setText(branch.name);
	tag.branchPhoneContact.setText(branch.contact.toString());

	// make the <10KM icon visible/invis depending on whether its near or not to the user
	if (Utils.isBranchNear(branch))
	    tag.near.setVisibility(View.VISIBLE);
	else
	    tag.near.setVisibility(View.INVISIBLE);

	// setup the background color; light-cyan/white
	if (position % 2 == 0)
	    convertView.setBackgroundColor(0x9999ffff);
	else
	    convertView.setBackgroundColor(Color.WHITE);
	return convertView;
    }

    /** For the alphabetical guide on the right side */
    private void setUpIndexer() {
	alphaIndexer = new HashMap<String, Integer>();
	headerPositions = new ArrayList<Integer>();
	for (int i = 0; i < branches.size(); i++) {
	    String s = branches.get(i).location.city.length() > 0 ? branches.get(i).location.city.substring(0, 1) : "";
	    if (!alphaIndexer.containsKey(s)) {
		alphaIndexer.put(s, i);
		headerPositions.add(i);
	    }
	}

	Set<String> sectionLetters = alphaIndexer.keySet();
	ArrayList<String> sectionList = new ArrayList<String>(sectionLetters);
	Collections.sort(sectionList);
	sections = new String[sectionList.size()];
	for (int i = 0; i < sectionList.size(); i++)
	    sections[i] = sectionList.get(i);
    }
}
