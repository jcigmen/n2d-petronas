package com.noobs2d.petronas.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.model.Marker;
import com.noobs2d.petronas.R;

/**
 * @author Julious Cious Igmen <jcigmen@gmail.com>
 */
public class BranchMapItemMarkerAdapter implements InfoWindowAdapter {

    private String address;

    private String contact;

    private LayoutInflater inflater;
    private String name;

    public BranchMapItemMarkerAdapter(LayoutInflater inflater, String name, String address, String contact) {
	this.inflater = inflater;
	this.address = address;
	this.contact = contact;
	this.name = name;
    }

    @Override
    public View getInfoContents(Marker marker) {
	View view = inflater.inflate(R.layout.layout_map_marker, null);

	((TextView) view.findViewById(R.id.markerBranchName)).setText(name);
	((TextView) view.findViewById(R.id.markerBranchAddress)).setText(address);
	((TextView) view.findViewById(R.id.markerBranchContact)).setText(contact);
	return view;
    }

    @Override
    public View getInfoWindow(Marker marker) {
	return null;
    }

}
