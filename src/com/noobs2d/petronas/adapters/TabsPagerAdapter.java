package com.noobs2d.petronas.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.noobs2d.petronas.fragments.BranchesFragment;
import com.noobs2d.petronas.fragments.NearMeFragment;
import com.noobs2d.petronas.fragments.ProductsFragment;

/**
 * @author Julious Cious Igmen <jcigmen@gmail.com>
 */
public class TabsPagerAdapter extends FragmentPagerAdapter {

    private BranchesFragment branchesFragment;
    private NearMeFragment nearMeFragment;
    private ProductsFragment productsFragment;

    public TabsPagerAdapter(FragmentManager fragmentManager) {
	super(fragmentManager);
	nearMeFragment = new NearMeFragment();
	productsFragment = new ProductsFragment();
	branchesFragment = new BranchesFragment();
    }

    public BranchesFragment getBranchesFragment() {
	return branchesFragment;
    }

    @Override
    public int getCount() {
	return 3;
    }

    @Override
    public Fragment getItem(int index) {
	switch (index) {
	    case 0:
		return nearMeFragment;
	    case 1:
		return branchesFragment;
	    case 2:
		return productsFragment;
	}
	return null; // must not happen
    }

    public NearMeFragment getNearMeFragment() {
	return nearMeFragment;
    }

    public ProductsFragment getProductsFragment() {
	return productsFragment;
    }

}
