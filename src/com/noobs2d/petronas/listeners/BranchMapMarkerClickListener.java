package com.noobs2d.petronas.listeners;

import java.util.ArrayList;

import android.view.LayoutInflater;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.model.Marker;
import com.noobs2d.petronas.adapters.BranchMapItemMarkerAdapter;
import com.noobs2d.petronas.models.Branch;

/**
 * @author Julious Cious Igmen <jcigmen@gmail.com>
 */
public class BranchMapMarkerClickListener implements OnMarkerClickListener {

    private ArrayList<Branch> branches;
    private LayoutInflater inflater;
    private GoogleMap map;

    public BranchMapMarkerClickListener(GoogleMap map, LayoutInflater inflater, ArrayList<Branch> branches) {
	this.branches = branches;
	this.inflater = inflater;
	this.map = map;
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
	Branch branch = getBranchByName(marker.getTitle());
	map.setInfoWindowAdapter(new BranchMapItemMarkerAdapter(inflater, branch.name, branch.location.toString(), branch.contact.toString()));
	return false;
    }

    private Branch getBranchByName(String name) {
	for (Branch branch : branches)
	    if (branch.name.equals(name))
		return branch;
	return null;
    }

}
