package com.noobs2d.petronas.listeners;

import java.util.ArrayList;

import com.noobs2d.petronas.models.Branch;

/**
 * @author Julious Cious Igmen <jcigmen@gmail.com>
 */
public interface BranchesRetrievalListener {

    public void onBranchesRetrieved(ArrayList<Branch> branches);

    public void onBranchesRetrieveFailed(Exception e);
}
