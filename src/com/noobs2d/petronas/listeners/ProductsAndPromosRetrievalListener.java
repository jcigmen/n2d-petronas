package com.noobs2d.petronas.listeners;

import java.util.ArrayList;

import com.noobs2d.petronas.models.Product;
import com.noobs2d.petronas.models.Promotion;

/**
 * @author Julious Cious Igmen <jcigmen@gmail.com>
 */
public interface ProductsAndPromosRetrievalListener {

    public void onProductsRetrieved(ArrayList<Promotion> promos, ArrayList<Product> products);

    public void onProductsRetrieveFailed(Exception e);
}
