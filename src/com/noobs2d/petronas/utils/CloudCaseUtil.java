package com.noobs2d.petronas.utils;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Julious Cious Igmen <jcigmen@gmail.com>
 */
public class CloudCaseUtil {

    public static final String API_HOST = "http://api.whatstreetapp.com/api/find";
    public static final String APP_ID = "DP.Petronas";
    public static final String APP_KEY = "261543a05555e380a0a22a111fc530bc";
    public static final String GUEST_KEY = "e5c4aa891fea19a94db526c2c911283f";
    public static final String GUEST_NAME = "whatstreet";

    public static String getBranchesJSONString() {
	JSONObject holder = null;
	JSONObject data = new JSONObject();

	try {
	    holder = getGenericObjectHolder();
	    holder.put("collection", "branches");
	    data.put("data", holder.toString());
	} catch (JSONException e) {
	    e.printStackTrace();
	}

	return data.toString();
    }

    public static String getProductsJSONString() {
	JSONObject holder = null;
	JSONObject data = new JSONObject();

	try {
	    holder = getGenericObjectHolder();
	    holder.put("collection", "products");
	    data.put("data", holder.toString());
	} catch (JSONException e) {
	    e.printStackTrace();
	}

	return data.toString();
    }

    public static String getPromotionsJSONString() {
	JSONObject holder = null;
	JSONObject data = new JSONObject();

	try {
	    holder = getGenericObjectHolder();
	    holder.put("collection", "promotions");
	    data.put("data", holder.toString());
	} catch (JSONException e) {
	    e.printStackTrace();
	}

	return data.toString();
    }

    public static String retrieveBranches() throws ClientProtocolException, IOException {
	return POST(getBranchesJSONString());
    }

    public static String retrieveProducts() throws ClientProtocolException, IOException {
	return POST(getProductsJSONString());
    }

    public static String retrievePromotions() throws ClientProtocolException, IOException {
	return POST(getPromotionsJSONString());
    }

    private static JSONObject getGenericObjectHolder() throws JSONException {
	JSONObject holder = new JSONObject();
	holder.put("app_id", APP_ID);
	holder.put("app_key", APP_KEY);
	holder.put("guest_access", "true");
	holder.put("guest_key", GUEST_KEY);
	holder.put("guest_name", GUEST_NAME);
	return holder;
    }

    private static String POST(String stringifiedJSON) throws ClientProtocolException, IOException {
	String result = "";

	HttpClient client = new DefaultHttpClient();
	StringEntity stringEntity = new StringEntity(stringifiedJSON);
	stringEntity.setContentType("application/json;charset=UTF-8");
	stringEntity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8"));

	HttpPost post = new HttpPost(CloudCaseUtil.API_HOST);
	post.setHeader("Accept", "application/json");
	post.setHeader("Content-type", "application/json");
	post.setEntity(stringEntity);

	HttpResponse response = client.execute(post);

	result = EntityUtils.toString(response.getEntity());

	return result;
    }
}
