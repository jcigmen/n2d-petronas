package com.noobs2d.petronas.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;

import com.noobs2d.petronas.fragments.NearMeFragment;
import com.noobs2d.petronas.models.Branch;

/**
 * @author Julious Cious Igmen <jcigmen@gmail.com>
 */
public class Utils {

    public static final float MAP_ZOOM_LEVEL = 12f;
    public static final int OUTLET_DISTANCE_RADIUS_METER = 10000;

    public static float getBranchDistance(android.location.Location userLocation, Branch branch) {
	if (userLocation != null) {
	    android.location.Location location = new android.location.Location("");
	    location.setLatitude(branch.location.latitude);
	    location.setLongitude(branch.location.longitude);
	    return userLocation.distanceTo(location);
	}
	return Long.MAX_VALUE;
    }

    public static boolean isBranchNear(Branch branch) {
	if (NearMeFragment.userLocation != null) {
	    android.location.Location location = new android.location.Location("");
	    location.setLatitude(branch.location.latitude);
	    location.setLongitude(branch.location.longitude);
	    if (NearMeFragment.userLocation.distanceTo(location) < OUTLET_DISTANCE_RADIUS_METER)
		System.out.println(branch.name + " - " + branch.location.city);

	    return NearMeFragment.userLocation.distanceTo(location) < OUTLET_DISTANCE_RADIUS_METER;
	}
	return false;
    }

    public static boolean isNetworkAvailable(Context context) {
	ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	NetworkInfo networkInfo = cm.getActiveNetworkInfo();
	if (networkInfo != null && networkInfo.isConnected())
	    return true;
	return false;
    }

    public static String loadBranches(Context context) {
	SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
	String branches = prefs.getString("BRANCHES", "");
	return branches;
    }

    public static String loadProducts(Context context) {
	SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
	String products = prefs.getString("PRODUCTS", "");
	return products;
    }

    public static String loadPromos(Context context) {
	SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
	String promos = prefs.getString("PROMOTIONS", "");
	return promos;
    }

    public static void saveBranches(Context context, String branchesJSONString) {
	SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
	Editor editor = prefs.edit();
	editor.putString("BRANCHES", branchesJSONString);
	editor.commit();
    }

    public static void saveProducts(Context context, String productsJSONString) {
	SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
	Editor editor = prefs.edit();
	editor.putString("PRODUCTS", productsJSONString);
	editor.commit();
    }

    public static void savePromos(Context context, String promosJSONString) {
	SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
	Editor editor = prefs.edit();
	editor.putString("PROMOTIONS", promosJSONString);
	editor.commit();
    }
}
