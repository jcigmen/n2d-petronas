package com.noobs2d.petronas.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import android.content.Context;
import android.os.AsyncTask;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.noobs2d.petronas.listeners.BranchesRetrievalListener;
import com.noobs2d.petronas.models.Branch;
import com.noobs2d.petronas.models.Branch.Contact;
import com.noobs2d.petronas.models.Branch.Location;
import com.noobs2d.petronas.utils.CloudCaseUtil;
import com.noobs2d.petronas.utils.Utils;

public class BranchesController {

    private class BranchLocationSortingComparator implements Comparator<Branch> {

	private android.location.Location userLocation;

	public BranchLocationSortingComparator(android.location.Location userLocation) {
	    this.userLocation = userLocation;
	}

	@Override
	public int compare(Branch lhs, Branch rhs) {
	    return (int) (Float.valueOf(Utils.getBranchDistance(userLocation, lhs)) - Float.valueOf(Utils.getBranchDistance(userLocation, rhs)));
	}
    }

    private class BranchSortingComparator implements Comparator<Branch> {

	@Override
	public int compare(Branch lhs, Branch rhs) {
	    return lhs.location.city.compareTo(rhs.location.city);
	}

    }

    private class RetrieveBranchesTask extends AsyncTask<Void, Void, String> {

	@Override
	protected String doInBackground(Void... params) {
	    String result = "";

	    try {
		result = CloudCaseUtil.retrieveBranches();
	    } catch (ClientProtocolException e) {
		e.printStackTrace();
	    } catch (IOException e) {
		e.printStackTrace();
	    } catch (Exception e) {
		e.printStackTrace();
	    }

	    return result;
	}

	@Override
	protected void onPostExecute(String result) {
	    extractBranchesDataFromJSONString(result);
	    Utils.saveBranches(context, result);
	}
    }

    private ArrayList<Branch> branches;

    private Context context;
    private BranchesRetrievalListener[] listeners;

    public BranchesController(Context context, BranchesRetrievalListener... listeners) {
	this.context = context;
	this.listeners = listeners;
	branches = new ArrayList<Branch>();
    }

    public ArrayList<Branch> getNearestBranches(android.location.Location userLocation) {
	ArrayList<Branch> sortedBranches = new ArrayList<Branch>();
	ArrayList<Branch> nearestBranches = new ArrayList<Branch>();
	sortedBranches.addAll(branches);
	Collections.sort(sortedBranches, new BranchLocationSortingComparator(userLocation));
	for (Branch branch : sortedBranches)
	    if (Utils.isBranchNear(branch))
		nearestBranches.add(branch);
	return nearestBranches;
    }

    public void sync() {
	if (Utils.isNetworkAvailable(context))
	    new RetrieveBranchesTask().execute();
	extractBranchesDataFromJSONString(Utils.loadBranches(context));
    }

    private void extractBranchesDataFromJSONString(String JSONString) {
	branches.clear();
	try {
	    JSONObject holder = new JSONObject(JSONString);
	    JSONArray data = (JSONArray) holder.get("collections");
	    for (int i = 0; i < data.length(); i++) {
		String id = "";
		String area = "";
		String city = "";
		String country = "";
		String district = "";
		String email = "";
		String mobile = "";
		String name = "";
		String phone = "";
		String region = "";
		String state = "";
		String street = "";
		String zipCode = "";
		float latitude = 0f;
		float longitude = 0f;
		try {
		    JSONObject object = new JSONObject(data.get(i).toString());
		    id = object.getString("_id");
		    area = object.getString("area");
		    city = object.getString("city");
		    country = object.getString("country");
		    district = object.getString("district");
		    email = object.getString("email");
		    mobile = object.getString("mobile");
		    name = object.getString("name");
		    phone = object.getString("phone");
		    region = object.getString("region");
		    state = object.getString("state");
		    street = object.getString("street");
		    zipCode = object.getString("zip");
		    latitude = Float.parseFloat(object.getString("latitude"));
		    longitude = Float.parseFloat(object.getString("longitude"));
		} catch (JSONException e) {
		    // one of the fields are probably empty
		}
		Contact contact = new Contact(email, mobile, phone);
		Location location = new Location(city, state, street, new float[] { latitude, longitude });
		location.area = area;
		location.country = country;
		location.district = district;
		location.region = region;
		location.zipCode = zipCode;
		Branch branch = new Branch(id, name, contact, location);
		branches.add(branch);
	    }
	    Collections.sort(branches, new BranchSortingComparator());
	    for (BranchesRetrievalListener listener : listeners)
		listener.onBranchesRetrieved(branches);
	} catch (JSONException e) {
	    e.printStackTrace();
	    for (BranchesRetrievalListener listener : listeners)
		listener.onBranchesRetrieveFailed(e);
	}
    }
}
