package com.noobs2d.petronas.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import android.content.Context;
import android.os.AsyncTask;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.noobs2d.petronas.listeners.ProductsAndPromosRetrievalListener;
import com.noobs2d.petronas.models.Product;
import com.noobs2d.petronas.models.Promotion;
import com.noobs2d.petronas.utils.CloudCaseUtil;
import com.noobs2d.petronas.utils.Utils;

/**
 * @author Julious Cious Igmen <jcigmen@gmail.com>
 */
public class ProductsAndPromosController {

    private class ProductComparator implements Comparator<Product> {

	@Override
	public int compare(Product lhs, Product rhs) {
	    return lhs.name.compareTo(rhs.name);
	}

    }

    private class PromoComparator implements Comparator<Promotion> {

	@Override
	public int compare(Promotion lhs, Promotion rhs) {
	    return lhs.name.compareTo(rhs.name);
	}

    }

    private class RetrieveProductsTask extends AsyncTask<Void, Void, String> {

	@Override
	protected String doInBackground(Void... params) {
	    String result = "";

	    try {
		result = CloudCaseUtil.retrieveProducts();
	    } catch (ClientProtocolException e) {
		e.printStackTrace();
	    } catch (IOException e) {
		e.printStackTrace();
	    } catch (Exception e) {
		e.printStackTrace();
	    }

	    return result;
	}

	@Override
	protected void onPostExecute(String result) {
	    extractProductsDataFromJSONString(result);
	    Utils.saveProducts(context, result);
	}
    }

    private class RetrievePromosTask extends AsyncTask<Void, Void, String> {

	@Override
	protected String doInBackground(Void... params) {
	    String result = "";
	    try {
		result = CloudCaseUtil.retrievePromotions();
	    } catch (ClientProtocolException e) {
		e.printStackTrace();
		listener.onProductsRetrieveFailed(e);
	    } catch (IOException e) {
		e.printStackTrace();
		listener.onProductsRetrieveFailed(e);
	    }
	    return result;
	}

	@Override
	protected void onPostExecute(String result) {
	    extractPromosDataFromJSONString(result);
	    Utils.savePromos(context, result);
	}

    }

    private Context context;
    private ProductsAndPromosRetrievalListener listener;

    private ArrayList<Product> products;

    private ArrayList<Promotion> promos;

    public ProductsAndPromosController(Context context, ProductsAndPromosRetrievalListener listener) {
	this.context = context;
	this.listener = listener;
	products = new ArrayList<Product>();
	promos = new ArrayList<Promotion>();
    }

    public ArrayList<Product> getProducts() {
	return products;
    }

    public void sync() {
	if (Utils.isNetworkAvailable(context))
	    new RetrieveProductsTask().execute();
	extractProductsDataFromJSONString(Utils.loadProducts(context));
    }

    private void extractProductsDataFromJSONString(String JSONString) {
	products.clear();
	try {
	    JSONObject holder = new JSONObject(JSONString);
	    JSONArray data = (JSONArray) holder.get("collections");
	    for (int i = 0; i < data.length(); i++) {
		String id = "";
		String category = Product.UNCATEGORIZED;
		String name = "";
		String subName = "";
		String description = "";
		String model = "";
		String image = "sample_product.png";
		String thumbnailFilename = "sample_product.png";
		try {
		    JSONObject object = new JSONObject(data.get(i).toString());
		    id = object.getString("_id");
		    name = object.getString("name");
		    subName = object.getString("subname");
		    description = object.getString("desc");
		    model = object.getString("model");
		    image = object.getString("image");
		    thumbnailFilename = object.getString("thumbnail").replace('-', '_').replace("jpeg", "png");
		    category = object.getString("category").replace('-', '_').replace("jpeg", "png");
		} catch (JSONException e) {
		    // one of the fields are probably empty
		}
		Product product = new Product(id, category, description, image, name, model, subName, thumbnailFilename);
		products.add(product);
	    }
	} catch (JSONException e) {
	    e.printStackTrace();
	    listener.onProductsRetrieveFailed(e);
	}
	if (Utils.isNetworkAvailable(context))
	    new RetrievePromosTask().execute();
	else
	    extractPromosDataFromJSONString(Utils.loadPromos(context));
    }

    private void extractPromosDataFromJSONString(String JSONString) {
	promos.clear();
	JSONObject holder;
	try {
	    holder = new JSONObject(JSONString);
	    JSONArray data = (JSONArray) holder.get("collections");
	    for (int i = 0; i < data.length(); i++) {
		String id = "";
		String name = "";
		String subName = "";
		String description = "";
		String permitNumber = "";
		String promoPeriod = "";
		String banner = "sample_promo.png";
		String thumbnailFilename = "sample_promo.png";
		try {
		    JSONObject object = (JSONObject) data.get(i);
		    id = object.getString("_id");
		    name = object.getString("name");
		    subName = object.getString("subname");
		    description = object.getString("desc");
		    permitNumber = object.getString("permit_number");
		    promoPeriod = object.getString("promo_period");
		    banner = object.getString("banner").replace('-', '_').replace("jpeg", "png");
		    thumbnailFilename = object.getString("thumbnail").replace('-', '_').replace("jpeg", "png");
		} catch (JSONException e) {
		    // one of the fields are probably empty
		}
		Promotion promo = new Promotion(id, name, subName, description, permitNumber, promoPeriod, banner, thumbnailFilename);
		promos.add(promo);
	    }
	    Collections.sort(products, new ProductComparator());
	    Collections.sort(promos, new PromoComparator());
	} catch (JSONException e) {
	    e.printStackTrace();
	    listener.onProductsRetrieveFailed(e);
	}
	listener.onProductsRetrieved(promos, products);
    }
}
