package com.noobs2d.petronas.models;

/** 
 * Datastructure describing a Branch.
 * 
 * @author Julious Cious Igmen <jcigmen@gmail.com>
 */
public class Branch {

    public static class Contact {

	public String email;
	public String mobile;
	public String phone;

	public Contact(String email, String mobile, String phone) {
	    this.email = email;
	    this.mobile = mobile;
	    this.phone = phone;
	}

	@Override
	public String toString() {
	    String contacts = phone + " " + mobile + " " + email;
	    return contacts.equals("  ") ? "No contacts available" : contacts;
	}
    }

    public static class Location {

	public String area = "";
	public String city = "";
	public String country = "";
	public String district = "";
	public float latitude;
	public float longitude;
	public String region = "";
	public String state = "";
	public String street = "";
	public String zipCode = "";

	public Location(String city, String state, String street, float[] latLon) {
	    this.city = city;
	    this.state = state;
	    this.street = street;
	    latitude = latLon[0];
	    longitude = latLon[1];
	}

	@Override
	public String toString() {
	    String address = !street.equals("") ? street : "";
	    address += !city.equals("") ? ", " + city : "";
	    return address.equals("") ? "No Address Available" : address;
	}
    }

    public Contact contact;
    public String id;
    public Location location;
    public String name;

    public Branch(String id, String name, Contact contact, Location location) {
	this.id = id;
	this.name = name;
	this.contact = contact;
	this.location = location;
    }
}
