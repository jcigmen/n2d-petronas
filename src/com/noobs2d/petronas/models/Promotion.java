package com.noobs2d.petronas.models;

import java.io.Serializable;

/**
 * Datastructure describing a Promo.
 * 
 * @author Julious Cious Igmen <jcigmen@gmail.com>
 */
public class Promotion implements Serializable {

    private static final long serialVersionUID = -837731928493863684L;

    public String banner;
    public String description;
    public String id;
    public String name;
    public String permitNumber;
    public String promoPeriod;
    public String subName;
    public String thumbnailFilename;

    public Promotion(String id, String name, String subName, String description, String permitNumber, String promoPeriod, String banner, String thumbnailFilename) {
	this.id = id;
	this.name = name;
	this.subName = subName;
	this.banner = banner;
	this.description = description;
	this.promoPeriod = promoPeriod;
	this.permitNumber = permitNumber;
	this.thumbnailFilename = thumbnailFilename;
    }
}
