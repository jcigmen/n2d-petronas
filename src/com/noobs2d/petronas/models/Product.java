package com.noobs2d.petronas.models;

import java.io.Serializable;

/**
 * @author Julious Cious Igmen <jcigmen@gmail.com>
 */
public class Product implements Serializable {

    public static final String DIESEL_ENGINE_OILS = "Diesel Engine Oils";
    public static final String GASOLINE_ENGINE_OILS = "Gasoline Engine Oils";
    public static final String MOTORCYCLE_OILS = "Motorcycle Oils";
    public static final String SPECIALTY = "Specialty Products";
    public static final String UNCATEGORIZED = "Uncategorized";

    private static final long serialVersionUID = 4870005700703317939L;

    public String category;
    public String description;
    public String id;
    public String image;
    public String model;
    public String name;
    public String subName;
    public String thumbnailFilename;

    public Product(String id, String category, String description, String image, String name, String model, String subName, String thumbnailFilename) {
	this.id = id;
	this.category = category;
	this.description = description;
	this.image = image;
	this.model = model;
	this.name = name;
	this.subName = subName;
	this.thumbnailFilename = thumbnailFilename;
    }
}
