package com.noobs2d.petronas;

import java.util.ArrayList;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.noobs2d.petronas.activities.SplashScreenActivity;
import com.noobs2d.petronas.adapters.TabsPagerAdapter;
import com.noobs2d.petronas.controllers.BranchesController;
import com.noobs2d.petronas.controllers.ProductsAndPromosController;
import com.noobs2d.petronas.fragments.NearMeFragment;
import com.noobs2d.petronas.listeners.BranchesRetrievalListener;
import com.noobs2d.petronas.models.Branch;

/** 
 * Where the ViewPager and tabs are instantiated and created.
 * Shows after {@link SplashScreenActivity}.
 * 
 * @author Julious Cious Igmen <jcigmen@gmail.com>
 */
public class MainActivity extends FragmentActivity implements OnPageChangeListener, TabListener, BranchesRetrievalListener {

    public static BranchesController branchesController;
    public static FragmentManager fragmentManager;
    public static NearMeFragment nearMeFragment;
    private ActionBar actionBar;
    private ProductsAndPromosController productsController;
    private MenuItem sync;
    private TabsPagerAdapter tabsPagerAdapter;
    private ViewPager viewPager;

    @Override
    public void onBranchesRetrieved(ArrayList<Branch> branches) {
	tabsPagerAdapter.getNearMeFragment().showNearestBranches();
	if (sync != null) {
	    sync.collapseActionView();
	    sync.setActionView(null);
	}
	Toast.makeText(this, getString(R.string.sync_success), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBranchesRetrieveFailed(Exception e) {
	if (sync != null) {
	    sync.collapseActionView();
	    sync.setActionView(null);
	}
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
	nearMeFragment = tabsPagerAdapter.getNearMeFragment();
	getMenuInflater().inflate(R.menu.main, menu);
	productsController = new ProductsAndPromosController(getApplicationContext(), tabsPagerAdapter.getProductsFragment());
	productsController.sync();
	branchesController = new BranchesController(getApplicationContext(), this, tabsPagerAdapter.getBranchesFragment());
	branchesController.sync();

	return true;
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
	if (item.getItemId() == R.id.action_sync) {
	    sync = item;
	    sync.setActionView(R.layout.indeterminate_progress_bar);
	    sync.expandActionView();
	    branchesController.sync();
	    productsController.sync();
	}
	return super.onMenuItemSelected(featureId, item);
    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {
    }

    @Override
    public void onPageScrollStateChanged(int arg0) {
    }

    @Override
    public void onPageSelected(int position) {
	actionBar.setSelectedNavigationItem(position);
    }

    @Override
    public void onTabReselected(Tab tab, FragmentTransaction fragmentTransaction) {
    }

    @Override
    public void onTabSelected(Tab tab, FragmentTransaction fragmentTransaction) {
	viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(Tab tab, FragmentTransaction fragmentTransaction) {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_main);

	fragmentManager = getSupportFragmentManager();

	actionBar = getActionBar();
	actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE | ActionBar.DISPLAY_SHOW_CUSTOM);
	actionBar.setHomeButtonEnabled(false);
	actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
	tabsPagerAdapter = new TabsPagerAdapter(getSupportFragmentManager());
	viewPager = (ViewPager) findViewById(R.id.pager);
	viewPager.setAdapter(tabsPagerAdapter);
	viewPager.setOnPageChangeListener(this);

	String[] tabs = { "Near Me", "Outlets", "Products" };
	for (String tabName : tabs)
	    actionBar.addTab(actionBar.newTab().setText(tabName).setTabListener(this));
    }
}
